package com.cheapfast.interfaces;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import com.cheapfast.domain.Response;
import com.cheapfast.domain.Schedule;
import com.cheapfast.domain.TestCase;

public interface ScheduleLoader {
	
	TestCase loadTestCase(BufferedReader bufferedReader) throws IOException;
	
	List<Response> calculateFastestRoute(TestCase testCase);

}
