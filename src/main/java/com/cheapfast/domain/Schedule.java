package com.cheapfast.domain;

import java.math.BigDecimal;
import java.time.LocalTime;

import com.cheapfast.util.Utils;

/*
 * The class which represents the schedule
 */
public class Schedule {
	
	private String source;
	
	private String destination;
	
	private LocalTime departure;
	
	private LocalTime arrival;
	
	private BigDecimal price;

	// This constructor creates the object from a String.
	// The String contains the field values separated by blank space
	public Schedule(String inputLine) {
		String[] tokens = inputLine.split(" ");
		source = tokens[0];
		destination = tokens[1];
		
		departure = Utils.retrieveTimeFromString(tokens[2]);
		
		arrival = Utils.retrieveTimeFromString(tokens[3]);
		
		price = new BigDecimal(tokens[4]);
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalTime getDeparture() {
		return departure;
	}

	public void setDeparture(LocalTime departure) {
		this.departure = departure;
	}

	public LocalTime getArrival() {
		return arrival;
	}

	public void setArrival(LocalTime arrival) {
		this.arrival = arrival;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrival == null) ? 0 : arrival.hashCode());
		result = prime * result
				+ ((departure == null) ? 0 : departure.hashCode());
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	// Overriding the equals method
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Schedule other = (Schedule) obj;
		if (arrival == null) {
			if (other.arrival != null)
				return false;
		} else if (!arrival.equals(other.arrival))
			return false;
		if (departure == null) {
			if (other.departure != null)
				return false;
		} else if (!departure.equals(other.departure))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		// We use compareTo instead of equals in BigDecimal since equals
		// takes into consideration the BigDecimal's scale
		} else if (price.compareTo(other.price) != 0)
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Schedule [source=" + source + ", destination=" + destination
				+ ", departure=" + departure + ", arrival=" + arrival
				+ ", price=" + price + "]";
	}
	
}
