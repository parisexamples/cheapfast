package com.cheapfast.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestCase {
	
	// Since the problem's solution breaks down to a DFS algorithm, it is handy to have a map which
	// contains every city and its "adjacent" cities. 
	private Map<String, List<Schedule>> schedules = new HashMap<String, List<Schedule>>();

	public Map<String, List<Schedule>> getSchedules() {
		return schedules;
	}

	public void setSchedules(Map<String, List<Schedule>> schedules) {
		this.schedules = schedules;
	}

	@Override
	public String toString() {
		return "TestCase [schedules=" + schedules + "]";
	}

}
