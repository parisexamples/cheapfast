package com.cheapfast.domain;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import com.cheapfast.util.Constants;

public class Response {
	
	private LocalTime departure;
	
	private LocalTime arrival;
	
	private BigDecimal price = new BigDecimal(0);
	
	// This class decides the format of the dates in the output
	private DateTimeFormatter localTimeFormatter;
	
	private int scale;
	
	// This constructor is taking the default output patterns for time and price
	public Response() {
		localTimeFormatter = DateTimeFormatter.ofPattern(Constants.DEFAULT_TIME_FORMAT);
		scale = Constants.DEFAULT_PRICE_SCALE;
	}

	// This constructor is able to set different values of the output pattern of time
	// and scale (in case it is needed)
	public Response(String localTimePattern, int scale) {
		localTimeFormatter = DateTimeFormatter.ofPattern(localTimePattern);
		this.scale = scale;
	}

	public Response(LocalTime departure, LocalTime arrival, BigDecimal price) {
		this.departure = departure;
		this.arrival = arrival;
		this.price = price;
		localTimeFormatter = DateTimeFormatter.ofPattern(Constants.DEFAULT_TIME_FORMAT);
		scale = Constants.DEFAULT_PRICE_SCALE;
	}
	
	public Response(LocalTime departure, LocalTime arrival, BigDecimal price, 
			String localTimePattern, int scale) {
		this.departure = departure;
		this.arrival = arrival;
		this.price = price;
		this.scale = scale;
		localTimeFormatter = DateTimeFormatter.ofPattern(localTimePattern);
		this.scale = scale;
	}

	public LocalTime getDeparture() {
		return departure;
	}

	public void setDeparture(LocalTime departure) {
		this.departure = departure;
	}

	public LocalTime getArrival() {
		return arrival;
	}

	public void setArrival(LocalTime arrival) {
		this.arrival = arrival;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	// Overriding the toString method to provide the required output for the output file
	@Override
	public String toString() {
		return departure.format(localTimeFormatter) + " " + arrival.format(localTimeFormatter)
				+ " " + price.setScale(scale);
	}
}
