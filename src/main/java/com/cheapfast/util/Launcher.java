package com.cheapfast.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.cheapfast.domain.Response;
import com.cheapfast.domain.Schedule;
import com.cheapfast.domain.TestCase;
import com.cheapfast.interfaces.ScheduleLoader;
import com.cheapfast.services.ScheduleLoaderImpl;

public class Launcher {
	/*
	 * Main's purpose is to load the input file, invoke the methods to produce the results and 
	 * output them to the output file
	 */
	public static void main(String[] args) {
		
		// Declaring basic variables for out main
		String inputFileName = "input.txt";
		String outputFileName = "output.txt";
		
		ScheduleLoader scheduleLoader = new ScheduleLoaderImpl();

		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = null;
		List<Response> listResponse = null;
		try {
			// Initializing the buffer reader for the input file and the buffer writer for the output file
			bufferedReader = Files.newBufferedReader(Paths.get(inputFileName));
			Path path = Paths.get(outputFileName);
			bufferedWriter = Files.newBufferedWriter(path);
			
			// First row contains the number of test cases - we trim for safety
			String buffer = bufferedReader.readLine();
			int testCaseNum = Integer.valueOf(buffer.trim());
			boolean firstResponse;
			
			List<TestCase> testCases = new ArrayList<TestCase>();
			// For each test case, parse it and create the corresponding object
			for(int i= 0; i < testCaseNum; i++) {
				
				// Loading the test case
				TestCase testCase = scheduleLoader.loadTestCase(bufferedReader);
				testCases.add(testCase);
				
				// Getting the responses. We also need to dump the to the output file
				// firstResponse boolean variable makes sure that we output the second result in a new line
				listResponse = scheduleLoader.calculateFastestRoute(testCase);
				firstResponse = true;
				// System.out.println(listResponse);
				for(Response response: listResponse) {
					// System.out.println(response);
					bufferedWriter.write(response.toString());
					if(firstResponse) {
						bufferedWriter.newLine();
					}
					firstResponse = false;
				}
				// The following check is done in order to leave a blank line between two responses and no
				// blank lines in the end - it is ugly but gets the job done!
				if(i < testCaseNum - 1) {
					bufferedWriter.newLine();
					bufferedWriter.newLine();
				}
			}
			
			
		} catch (IOException e) {
			System.out.println("Error while trying to read input file");
			e.printStackTrace();
		} finally {
			try {
				// In any case  (successful or unsuccessful run), we need to close the buffered reader/writer
				if(bufferedReader != null) {
					bufferedReader.close();
				}
				if( bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (IOException e) {
				System.out.println("Unable to close the input file stream");
				e.printStackTrace();
			}
		}
	}
}
