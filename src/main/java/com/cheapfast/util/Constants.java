package com.cheapfast.util;

public class Constants {
	
	public static final String SOURCE = "A";
	
	public static final String FINAL_DESTINATION = "Z";

	public static final String DEFAULT_TIME_FORMAT = "HH:mm";
	
	public static final int DEFAULT_PRICE_SCALE = 2;
	
	public static final String INPUT_FILE_PATTERN = "input.*.txt";
}
