package com.cheapfast.util;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

import com.cheapfast.domain.Schedule;

public class Utils {
	
	// The format of the arrival and departure time is: hh:MM
	// Therefore we split the value using : as delimiter. Seconds
	// and nanoseconds are by default set to 0
	public static LocalTime retrieveTimeFromString(String time) {
		String[] timeTokens = time.split(":");
		int hour = Integer.valueOf(timeTokens[0]);
		int minute = Integer.valueOf(timeTokens[1]);
		return LocalTime.of(hour, minute);
	}
	
	// calculate the price of a route
	public static BigDecimal calculateRoutePrice(List<Schedule> route) {
		BigDecimal price = new BigDecimal(0);
		for(Schedule schedule: route) {
			price = price.add(schedule.getPrice());
		}
		return price;
	}

}
