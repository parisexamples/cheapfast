package com.cheapfast.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cheapfast.domain.Response;
import com.cheapfast.domain.Schedule;
import com.cheapfast.domain.TestCase;
import com.cheapfast.interfaces.ScheduleLoader;
import com.cheapfast.util.Constants;
import com.cheapfast.util.Utils;

public class ScheduleLoaderImpl implements ScheduleLoader {

	/*
	 * This method reads a test case from the input file. A test case is
	 * a series of routes between cities. Outputs an instant of TestCase
	 * class. It is essentially a map with the cities as  keys and a list of
	 * the routes which start from this city as value
	 */
	@Override
	public TestCase loadTestCase(BufferedReader bufferedReader)
			throws IOException {
		String buffer;
		TestCase testCase = new TestCase();
		// First row is empty;
		bufferedReader.readLine();

		// This is the number of schedules of the current test case
		buffer = bufferedReader.readLine().trim();
		int scheduleNum = Integer.valueOf(buffer);
		Map<String, List<Schedule>> schedules = testCase.getSchedules();
		// Time to read and create schedule objects
		for (int j = 0; j < scheduleNum; j++) {
			buffer = bufferedReader.readLine().trim();
			Schedule schedule = new Schedule(buffer);
			// Insert the schedule to the test case's HashMap
			List<Schedule> scheduleList = schedules.get(schedule.getSource());
			if (scheduleList == null) {
				scheduleList = new ArrayList<Schedule>();
			}
			scheduleList.add(schedule);
			schedules.put(schedule.getSource(), scheduleList);
		}
		return testCase;
	}

	/*
	 * This method produces a List (always two items) of solutions (cheap and fast).
	 * If we view the cities as nodes of a graph and the plane flights as edges, we get a graph.
	 * Additionally the problem can be reduced to  two problems. Finding the cheapest route between
	 * A and Z is equal to finding the least weighted path from A to Z if we assign as weight for each
	 * edge the cost of the corresponding flight. Moreover, Finding the fastest route between A and Z
	 * is equal to finding the least weighted path from A to Z if we assign as weight for each
	 * edge the duration of the corresponding flight.
	 * As a result we need a graph traversal algorith. Depth First Search would suffice.
	 */
	@Override
	public List<Response> calculateFastestRoute(TestCase testCase) {

		List<Schedule> candidateRoute = new ArrayList<Schedule>();
		LocalTime currentTime = LocalTime.MIN;

		List<List<Schedule>> currentRoute = null;
		Response currentResp = null;
		Response newResp = null;
		// The currentRoute List contains the responses. First and second place contains the cheapest 
		// and fastest routes correspondingly.
		currentRoute = retrieveNextNode(candidateRoute, testCase,
				Constants.SOURCE, currentTime, currentRoute, newResp, currentResp);
		
		// constructing the cheap response
		List<Schedule> outputSchedule = currentRoute.get(0);
		LocalTime outputDeparture = outputSchedule.get(0).getDeparture();
		LocalTime outputArrival = outputSchedule.get(outputSchedule.size() - 1).getArrival();
		Response cheapResponse = new Response(outputDeparture, outputArrival, 
				Utils.calculateRoutePrice(outputSchedule));
		
		// constructing the fast response
		outputSchedule = currentRoute.get(1);
		outputDeparture = outputSchedule.get(0).getDeparture();
		outputArrival = outputSchedule.get(outputSchedule.size() - 1).getArrival();
		Response fastResponse = new Response(outputDeparture, outputArrival, 
				Utils.calculateRoutePrice(outputSchedule));
		List<Response> responses = new ArrayList<Response>();
		responses.add(cheapResponse);
		responses.add(fastResponse);
		return responses;
	}

	/*
	 * This method implements the DFS algorithm. The input variables are:
	 * 	- visitedNode: the list of flights that have beed traverser until now
	 *		by the algorithm. Needed to calculate cost and speed of the route,
	 *		as well as to avoid circles.
	 *	- testCase: essentially a map of each city and the flight that depart
	 *		from it. Loaded from befor to avoid to parse all the flights again.
	 *	- currentNode: the current node in the graph traversal procedure
	 *	- currentTime: Used to choose the next possible destination
	 *	- currentRoute: the paths (flights) of the current solutions
	 *	- newResp: The response that results from reaching city Z
	 *		in the DFS algorithm
	 *	- currentResp: The current response
	 */
	private List<List<Schedule>> retrieveNextNode(List<Schedule> visitedNodes,
			TestCase testCase, String currentNode, LocalTime currentTime,
			List<List<Schedule>> currentRoute, Response newResp, Response 
			currentResp) {

		// retrieve the next candidate destinations
		Map<String, List<Schedule>> schedules = testCase.getSchedules();
		List<Schedule> adjacentNodes = schedules.get(currentNode);

		for (Schedule candidateSchedule : adjacentNodes) {
			// for a candidate next route to be valid, it needs to have
			// departure time after the arrival time of the current node and not to have been visited
			// again. If the node has been visited again, we would not achieve better time
			// (positive duration  of flights) and we would have an endless loop
			if (currentTime.isBefore(candidateSchedule.getDeparture())
					&& (!visitedNodes.contains(candidateSchedule
							.getDestination()))) {
				currentTime = candidateSchedule.getArrival();
				currentNode = candidateSchedule.getDestination();
				visitedNodes.add(candidateSchedule);
				// check if this is the final destination
				if (candidateSchedule.getDestination().equals(
						Constants.FINAL_DESTINATION)) {
					if(currentRoute == null) {
						// first time we meet the "Z" destination - initialize the currentResponse (avoid NPE)
						currentRoute = new ArrayList<List<Schedule>>();
						currentRoute.add(null);
						currentRoute.add(null);
					}
					currentRoute.set(0, chooseRoute(visitedNodes, currentRoute.get(0), false, newResp, currentResp));
					currentRoute.set(1, chooseRoute(visitedNodes, currentRoute.get(1), true, newResp, currentResp));
					return currentRoute;
				} else {
					// recursion!!!
					currentRoute = retrieveNextNode(visitedNodes, testCase,
							currentNode, currentTime, currentRoute, newResp, currentResp);
					// Clean the visited nodes (routes) up until the current one to continue the DFS algorithm
					while(visitedNodes.contains(candidateSchedule)) {
						visitedNodes.remove(visitedNodes.size()-1);	
					}
					// Reset the current time variable
					currentTime = (visitedNodes.size() == 0)?LocalTime.MIN:visitedNodes.get(visitedNodes.size()-1).getArrival();
				}
			}
		}
		return currentRoute;
	}

	/*
	 * This method chooses the fastest or cheapest route depending on the value of the speed boolean variable
	 */
	private List<Schedule> chooseRoute(List<Schedule> newResponse, List<Schedule> currentResponse, boolean speed, 
			Response newCandidate, Response currentCandidate) {
		newCandidate = new Response(newResponse.get(0).getDeparture(), 
				newResponse.get(newResponse.size() - 1).getArrival(), 
				Utils.calculateRoutePrice(newResponse));
		
		if (currentResponse == null) {
			newCandidate = new Response(newResponse.get(0).getDeparture(), 
					newResponse.get(newResponse.size() - 1).getArrival(), 
					Utils.calculateRoutePrice(newResponse));
			return new ArrayList<Schedule>(newResponse);
		}
		
		currentCandidate = new Response(currentResponse.get(0).getDeparture(), 
				currentResponse.get(currentResponse.size() - 1).getArrival(), 
				Utils.calculateRoutePrice(currentResponse));
		// Estimate which route is faster and return it - estimate the cost too
		// as a tie breaker
		Duration newDuration = Duration.between(newCandidate.getDeparture(), newCandidate.getArrival());
		Duration currentDuration = Duration.between(currentCandidate.getDeparture(), currentCandidate.getArrival());
		BigDecimal currentPrice = currentCandidate.getPrice();
		BigDecimal newPrice = newCandidate.getPrice();
		
		int priceComparison = newPrice.compareTo(currentPrice);
		int durationComparison = newDuration.compareTo(currentDuration);
		
		if(speed) {
			// We want the fastest route
			if (durationComparison < 0) {
				return new ArrayList<Schedule>(newResponse);
			} else if (durationComparison == 0) {
				// Same duration of flights
				if (newPrice.compareTo(currentPrice) < 0) {
					return new ArrayList<Schedule>(newResponse);
				} else {
					return new ArrayList<Schedule>(currentResponse);
				}
			} else {
				return new ArrayList<Schedule>(currentResponse);
			}
		} else {
			if (priceComparison < 0) {
				return new ArrayList<Schedule>(newResponse);
			} else if( priceComparison > 0){
				return new ArrayList<Schedule>(currentResponse);
			} else {
				// Same cost of flights
				if (durationComparison < 0) {
					return new ArrayList<Schedule>(newResponse);
				} else {
					return new ArrayList<Schedule>(currentResponse);
				}
			}
		}

	}
}
